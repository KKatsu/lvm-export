FROM golang:1.16-alpine AS builder

WORKDIR /app

COPY go.mod ./
COPY go.sum ./

ENV PATH=/usr/local/go/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
ENV GOLANG_VERSION=1.16.5

ENV GOPATH=/go
ENV PATH=/go/bin:/usr/local/go/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

RUN go mod download

COPY *.go ./
COPY . .

RUN go build -o /lvm-export

FROM alpine
WORKDIR /app
COPY --from=builder ./lvm-export /app

EXPOSE 9845

ENTRYPOINT ./lvm-export
